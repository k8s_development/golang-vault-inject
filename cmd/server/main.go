package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"gitlab.com/k8s_development/golang-vault-inject/pkg/notes"
)


func init() {

	log.Println("start")
	log.Println("start2")
	files, err := os.ReadDir("/")
    if err != nil {
        log.Fatal(err)
    }

    for _, file := range files {
        fmt.Println(file.Name(), file.IsDir())
    }

}

func main() {
	mux := http.NewServeMux()

	noteService := notes.NewService()
	noteManager := notes.NewNoteHTTPHandler(noteService)

	mux.HandleFunc("POST /notes", noteManager.HandleHTTPPost)
	mux.HandleFunc("GET /notes", noteManager.HandleHTTPGet)
	mux.HandleFunc("GET /notes/{id}", noteManager.HandleHTTPGetWithID)
	mux.HandleFunc("PUT /notes/{id}", noteManager.HandleHTTPPut)
	mux.HandleFunc("DELETE /notes/{id}", noteManager.HandleHTTPDelete)

	err := http.ListenAndServe("0.0.0.0:8080", mux)
	if err != nil {
		panic(err)
	}
}